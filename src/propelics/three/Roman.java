package propelics.three;


import java.util.TreeMap;

public class Roman {
	private final  TreeMap<Integer, String> map;

	public Roman() {
		map = new TreeMap<Integer, String>();
		map.put(1000, "M");
        map.put(900, "CM");
        map.put(500, "D");
        map.put(400, "CD");
		map.put(100, "C");
		map.put(90, "XC");
		map.put(50, "L");
		map.put(40, "XL");
		map.put(10, "X");
		map.put(9, "IX");
		map.put(5, "V");
		map.put(4, "IV");
		map.put(1, "I");
	}

	public String solve(int number) {
		if (number < 1 || number > 3999)
			return "Number not valid";
		if (map.containsKey(number))
			return (String) map.get(number);
		return convert(number);
	}
	
	private String convert(int number) {
		// The method TreeMap#floorKey has a vital role in this solution as you can conveniently 
		// lookup the greatest key less than or equal to the given key. If there is an exact 
		// match you just return the associated roman symbol
        int l =  map.floorKey(number);
        if ( number == l ) {
            return map.get(number);
        }
        return map.get(l) + convert(number-l);
    }
	
	public String solve2(int mInt) {
	    String[] rnChars = { "M",  "CM", "D", "C",  "XC", "L",  "X", "IX", "V", "I" };
	    int[] rnVals = {  1000, 900, 500, 100, 90, 50, 10, 9, 5, 1 };
	    String retVal = "";
	    // A little more mortal code looping for all the possible symbols and matching
	    for (int i = 0; i < rnVals.length; i++) {
	        int numberInPlace = mInt / rnVals[i];
	        if (numberInPlace == 0) continue;
	        retVal += numberInPlace == 4 && i > 0? rnChars[i] + rnChars[i - 1]:
	            new String(new char[numberInPlace]).replace("\0",rnChars[i]);
	        mInt = mInt % rnVals[i];
	    }
	    return retVal;
	}

}
