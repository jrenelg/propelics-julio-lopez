package propelics.one;

import java.util.ArrayList;
import java.util.List;

public class Parentheses {
	private List<String> result;
	
	public Parentheses() {
		result = new ArrayList<String>();
	}

	public List<String> solve(int n) {
		combination("",n,0);
		return result;
	}
	
	/*
	 *  This recursive function will be invoked based in the follow criteria:
	 *  - We will have always the same (n) quantity of open and close symbols.
	 *  - Go through the binary tree in order to fill all the possible combinations
	 * */
	private void combination(String combination, int open, int close) {
        if (open > 0) 
        		combination(combination + "(", open-1, close + 1);
        if (close > 0)
        		combination(combination + ")", open, close - 1);
        if (open == 0 && close == 0)
            result.add(combination);
	}
	
}