package propelics.six;

public class Fibonacci {

	public String solve(int number) {
		int a = 0, b = 0, c = 1;
		String result = "";
		// Typical loop to calculate fobonnaci, is there some faster solution using arithmetic functions, but this covers
		// properly print each number
		for (int i = 1; i <= 100; i++) {
			a = b;
			b = c;
			c = a + b;
			result += a + " ";
		}
		return result;
	}
}
