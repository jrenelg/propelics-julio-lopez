package propelics.main;

import propelics.five.SumNoPlus;
import propelics.four.Binary;
import propelics.one.Parentheses;
import propelics.seven.Angle;
import propelics.six.Fibonacci;
import propelics.three.Roman;
import propelics.tools.Tool;
import propelics.two.Cross;

public class Ppal {

	public static final int N_FOR_PROBLEM_ONE = 2; // N-pair of parentheses for problem 1
	public static final int[][] MATRIX_PROBLEM_TWO = { { 1, 2, 3, 1 }, { 4, 5, 6, 3 }, { 7, 8, 9, 4 }, { 7, 0, 9, 4 } }; // Input
																															// Matrix
																															// for
																															// problem
																															// 2
	public static final int NUMBER_FOR_PROBLEM_THREE = 1956; // Number to be transform in Roman for problem 3
	public static final float NUMBER_FOR_PROBLEM_FOUR = 0.9f; // Number to be transform in Roman for problem 3
	public static final int NUMBER1_FOR_PROBLEM_FIVE = 8; // Number to be transform in Roman for problem 3
	public static final int NUMBER2_FOR_PROBLEM_FIVE = 2; // Number to be transform in Roman for problem 3
	public static final int NUMBER_FOR_PROBLEM_SIX = 100; // 100 first Fibo numbers
	public static final double HOURS_FOR_PROBLEM_SEVEN = 9; // Hours
	public static final double MINUTES_FOR_PROBLEM_SEVEN = 60; // Minutes
	
	
	public static void main(String[] args) {
		// Problem 1- Combinations of n-pairs of parentheses
		Parentheses parentheses = new Parentheses();
		System.out.println(parentheses.solve(N_FOR_PROBLEM_ONE));
		// Problem 2 - MxN matrix with 0 cross
		Cross cross = new Cross(MATRIX_PROBLEM_TWO);
		Tool.printMatrix(cross.solve());
		// Problem 3 - Number into a Roman Numeral 
		Roman roman = new Roman();
		System.out.println(roman.solve2(NUMBER_FOR_PROBLEM_THREE));
		// Problem 4 - Binary representation of Number btwn 0-1
		Binary binary = new Binary();
		System.out.println(binary.solve(NUMBER_FOR_PROBLEM_FOUR));
		// Problem 5 - Sum without use plus operator
		SumNoPlus sumNoPlus = new SumNoPlus();
		System.out.println(sumNoPlus.solve(NUMBER1_FOR_PROBLEM_FIVE, NUMBER2_FOR_PROBLEM_FIVE));
		// Problem 6 - First 100 Fibonacci numbers.
		Fibonacci fibonacci = new Fibonacci();
		System.out.println(fibonacci.solve(NUMBER_FOR_PROBLEM_SIX));
		// Problem 6 - Angle in analog clock
		Angle angle = new Angle();
		System.out.println(angle.solve(HOURS_FOR_PROBLEM_SEVEN,MINUTES_FOR_PROBLEM_SEVEN));
	}

}
