package propelics.four;

public class Binary {
	public String solve(float number) {
		if(number< 0 || number > 1)
			return "Invalid Number";
		// Use JDK function in order to parse number to Binary
		int intBits = Float.floatToIntBits(number);
		String intBitsStr = Integer.toBinaryString(intBits);
		if(intBitsStr.length() > 32)
			return "Error";
		return intBitsStr;
	}
}
