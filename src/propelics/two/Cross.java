package propelics.two;

public class Cross {
	int [ ] [ ] original;
	Integer [ ] [ ] result; // the object Integer instead of int native is important due to the null initialization
	
	public Cross(int [ ] [ ] input) {
		result = new Integer [input.length] [input.length];
		original = input;
	}

	// Looping through all the matrix and validating 0 position
	public Integer [ ] [ ]  solve() {
		for (int row = 0; row < original.length; row++) {
			for (int col = 0; col < original[row].length; col++) {
				result[row][col] =(result[row][col] == null) ? original[row][col] : result[row][col];
				if (original[row][col]==0)
					markCross(row, col);
			}
		}
		return result;
	}
	
	// funtion to print the cross in final response
	private void markCross(int prmRow, int prmCol) {
		for (int col = 0; col < original[prmRow].length; col++) 
			result[prmRow][col] = 0;
		for (int row = 0; row < original.length; row++)
			result[row][prmCol] = 0;
	}
}
