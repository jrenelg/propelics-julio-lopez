package propelics.five;

public class SumNoPlus {
	public Integer solve(int val1, int val2) {
		// Iterate till there is no carry
		while (val2 != 0) {
			// carry now contains common
			// set bits of val1 and val2
			int carry = val1 & val2;
			// Sum of bits of x and
			// y where at least one
			// of the bits is not set
			val1 = val1 ^ val2;
			// Carry is shifted by
			// one so that adding it
			// to x gives the required sum
			val2 = carry << 1;
		}
		return val1;
	}
}


/* Recursive way......
 int Add(int x, int y)
{
    if (y == 0)
        return x;
    else
        return Add( x ^ y, (x & y) << 1);
}
 */